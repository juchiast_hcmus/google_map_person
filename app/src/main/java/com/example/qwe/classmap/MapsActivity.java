package com.example.qwe.classmap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    public ArrayList<Person> getPersons() {
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(Person.Bao());
        persons.add(Person.Duy());
        persons.add(Person.Hy());
        persons.add(Person.Cuong());
        persons.add(Person.Nu());
        return persons;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        for (Person p : getPersons()) {
            p.addMarker(map);
        }
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(Person.camera_position, Person.camera_zoom));
        map.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Intent intent = new Intent(this, PersonActivity.class);
        intent.putExtra("person", (Person) marker.getTag());
        startActivity(intent);
        return false;
    }
}
