package com.example.qwe.classmap;

import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

public class Person implements Serializable {
    static public final LatLng camera_position = new LatLng(10.7625196, 106.6807066);
    static public final float camera_zoom = 16;

    private String name;
    private String facebook;
    private String phone;
    private int picture;
    private int icon;
    private double position_latitude;
    private double position_longitude;

    private Person() {
    }

    public void addMarker(GoogleMap map) {
        MarkerOptions opt = new MarkerOptions();
        opt.position(new LatLng(position_latitude, position_longitude));
        opt.icon(BitmapDescriptorFactory.fromResource(icon));
        Marker marker = map.addMarker(opt);
        marker.setTag(this);
    }

    public void bindData(PersonActivity view) {
        ImageView imageView = view.findViewById(R.id.picture);
        imageView.setImageResource(picture);

        TextView nameView = view.findViewById(R.id.name);
        nameView.setText(name);

        TextView phoneView = view.findViewById(R.id.phone);
        phoneView.setText(phone);

        TextView fbView = view.findViewById(R.id.facebook);
        fbView.setText(facebook);
    }

    public static Person Bao() {
        Person p = new Person();
        p.name = "Dương Nguyễn Thái Bảo";
        p.facebook = "fb.com/dntbao";
        p.phone = "0931321000";
        p.picture = R.drawable.bao;
        p.icon = R.drawable.icon_bao;
        p.position_latitude = 10.764360;
        p.position_longitude = 106.681909;
        return p;
    }

    public static Person Duy() {
        Person p = new Person();
        p.name = "Đỗ Hoàng Anh Duy";
        p.facebook = "fb.com/h2so4.rs";
        p.phone = "01259222222";
        p.picture = R.drawable.duy;
        p.icon = R.drawable.icon_duy;
        p.position_latitude = 10.763293;
        p.position_longitude = 106.679256;
        return p;
    }

    public static Person Cuong() {
        Person p = new Person();
        p.name = "Trần Quốc Cường";
        p.facebook = "fb.com/qcuong98";
        p.phone = "0998724000";
        p.picture = R.drawable.cuong;
        p.icon = R.drawable.icon_cuong;
        p.position_latitude = 10.762396;
        p.position_longitude = 106.681201;
        return p;
    }

    public static Person Nu() {
        Person p = new Person();
        p.name = "Hoàng Thiên Nữ";
        p.facebook = "fb.com/thiennust274";
        p.phone = "0999887000";
        p.picture = R.drawable.nu;
        p.icon = R.drawable.icon_nu;
        p.position_latitude = 10.760857;
        p.position_longitude = 106.679811;
        return p;
    }

    public static Person Hy() {
        Person p = new Person();
        p.name = "Vương Hy";
        p.facebook = "fb.com/hy.vuong.31";
        p.phone = "0912387000";
        p.picture = R.drawable.hy;
        p.icon = R.drawable.icon_hy;
        p.position_latitude = 10.762206;
        p.position_longitude = 106.682724;
        return p;
    }
}
