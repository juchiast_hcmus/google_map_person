package com.example.qwe.classmap;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class PersonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        Person person = (Person) getIntent().getSerializableExtra("person");
        person.bindData(this);
    }
}
