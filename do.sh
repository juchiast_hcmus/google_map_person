#!/usr/bin/bash
cp $1 icon_$1

convert icon_$1 \
 \( +clone  -alpha extract \
    -draw 'fill black polygon 0,0 0,84 84,0 fill white circle 84,84 84,0' \
    \( +clone -flip \) -compose Multiply -composite \
    \( +clone -flop \) -compose Multiply -composite \
 \) -alpha off -compose CopyOpacity -composite  icon_$1

convert -resize $2 icon_$1 icon_$1

convert $1 \
 \( +clone  -alpha extract \
    -draw 'fill black polygon 0,0 0,15 15,0 fill white circle 15,15 15,0' \
    \( +clone -flip \) -compose Multiply -composite \
    \( +clone -flop \) -compose Multiply -composite \
 \) -alpha off -compose CopyOpacity -composite  $1
